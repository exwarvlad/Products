require 'net/http'
require 'json'

# метод делает Post-запрос к сайту и возвращает id
def post_request(url)
  uri = URI.parse(url)

  hash = {test: 'TEST'}

  # отправляю Post-запрос
  response = Net::HTTP.post_form(uri, hash)

  # возвращаю id c ответа сервера
  JSON.parse(response.body)["id"]
end