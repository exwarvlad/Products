require 'open-uri'
require 'nokogiri'
require 'json'

# Класс генерирующий случайный url картинки с сайта http://jsonplaceholder.typicode.com/photos
# для работы Мейлера

class RandomUrl
  attr_reader :random_url, :thumbnail_url

  def initialize
    @uri = "http://jsonplaceholder.typicode.com/photos/"

    begin
      @page = Nokogiri::HTML(open(@uri))
    rescue SocketError
      abort "Нет сети"
    end

    # хэш с картинками
    @doc = JSON.parse(@page)
  end

  # метод вытаскивает случайный урл картинки и её thumbnailUrl
  def get_randoms_urls
    doc_size = @doc.size
    random_count = rand(doc_size)
    random_hash = @doc[random_count]

    @random_url = random_hash["url"]
    @thumbnail_url = random_hash["thumbnailUrl"]
  end

  # метод проверяет валидность урла
  def valid_url?(random_url, thumbnail_url)
    if random_url.is_a?(String) && thumbnail_url.is_a?(String)

      # нахожу последние 6 символов урлов, которые значение цвета в шестнадцатиричном виде
      last_6_symbol_rand_url = random_url.reverse[0..5].reverse
      last_6_symbol_thumb_url = thumbnail_url.reverse[0..5].reverse

      # проверка велечин цветов в Fixnum формате
      if last_6_symbol_thumb_url.hex > last_6_symbol_rand_url.hex
        false
      else
        true
      end
    else
      false
    end
  end

end