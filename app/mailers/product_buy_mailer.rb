class ProductBuyMailer < ApplicationMailer

  # отправка письма при удачной покупке
  def buy(url, email)
    @url = url

    mail to: email
  end

  # отправка письма, если покупка не удалась
  def admins_support(email_client, admin_email)
    @email_client = email_client

    mail to: admin_email
  end

  # отправка id админам
  def admins_notification_id(id, email)
    @id = id

    mail to: email
  end

end
