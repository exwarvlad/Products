class ApplicationMailer < ActionMailer::Base
  default from: "product@stajer.com"
  layout 'mailer'
end
