module ApplicationHelper

  # проверяет валидность продукта для покупки
  def product_valid?(product)
    if product.user.name.nil?
      false
    else
      true
    end
  end

  # метод для корректировки стиля, возвращает 'col-md-6'
  def get_style_img(products)
    if products.is_a?(Fixnum) && products > 1
      'col-md-6'
    end
  end
end
