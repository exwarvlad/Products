class UsersController < ApplicationController
  # посылаю незалогиненного пользователя
  before_action :authenticate_user!, except: [:show]

  # задаю объект @user для шаблонов и экшенов
  before_action :set_current_user, except: [:show]

  def new
    @user = current_user.build
  end

  def show
    @user = User.find(params[:id])
  end

  private

  def set_current_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(:name, :email, :magaz_avatar)
  end

end
