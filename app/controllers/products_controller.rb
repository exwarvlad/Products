require 'post_request'

class ProductsController < ApplicationController

  # задаю объект @product для экшена show
  before_action :set_product, only: [:show]

  # задаю объект @product от текущего юзера
  before_action :set_current_user_product, only: [:edit, :update, :destroy]

  def index
    @products = Product.all
    @bundles = Bundle.all
  end

  def new
    @product = current_user.products.build
  end

  def create
    @product = current_user.products.build(product_params)

    if @product.save
      redirect_to @product, notice: I18n.t('controllers.products.created')
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @product.update(product_params)
      redirect_to @product, notice: I18n.t('controllers.products.update')
    else
      render :edit
    end
  end

  def destroy
    @product.destroy
    redirect_to products_path, notice: I18n.t('controllers.products.destroyed')
  end

  # отвечает за то, что админ может обновлять статус продукта
  def admin
    if current_admin.present?
      @product = Product.find(params[:id]) # нахожу текущий продукт
      @product.update_status!
      redirect_to product_path, notice: I18n.t('controllers.products.status_update')
    end
  end

  # Экшен, который обрабатывает покупки пользователей (Guest)
  def price_product
    @product = Product.find(params[:id]) # нахожу текущий продукт
    if current_guest.present? && !@product.user.name.nil? && @product.pro == false

      # проверка мейла пользователя, если email не в зоне com, то посылает с сообщением об ошибке
      unless @product.email_valid?(current_guest)
        return redirect_to product_path(@product), alert: I18n.t('controllers.products.email_error')
      end

      # достаю продукт
      @select_product = @product.select_product

      # достаю email клиента
      @client_email = current_guest.email

      # если продукт бракованный, отправляю сообщение админам на почту и сообщаю пользователю об ошибке
      if @select_product == -1
        # уведомляю всех админов об ошибке
        notify_admins(@client_email)
        return redirect_to product_path(@product), alert: I18n.t('controllers.products.product_defect')
      end


      # отправляю продукт
      ProductBuyMailer.buy(@select_product, @client_email).deliver_now

      # делаю Post-запрос и забираю id-шник
      @id = post_request("http://jsonplaceholder.typicode.com/todos")

      # уведомляю всех админов об @id
      notify_admins_id(@id)

      redirect_to product_path(@product), notice: I18n.t('controllers.products.send_mail_complite')
    end
  end

  private

  def set_current_user_product
    @product = current_user.products.find(params[:id])
  end

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:title, :description, :photo)
  end

  def notify_admins(email_client)
    # собираю мейлы всех админов, исключая повторяющиеся
    all_admins_emails = Admin.all.map(&:email).uniq

    all_admins_emails.each do |admin_email|
      ProductBuyMailer.admins_support(email_client, admin_email).deliver_now
    end
  end

  def notify_admins_id(id)
    # собираю мейлы всех админов, исключая повторяющиеся
    all_admins_emails = Admin.all.map(&:email).uniq

    all_admins_emails.each do |admin_email|
      ProductBuyMailer.admins_notification_id(id, admin_email).deliver_now
    end
  end

end
