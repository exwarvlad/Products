class GuestsController < ApplicationController

  private

  def guest_params
    params.require(:guest).permit(:name, :email)
  end

end
