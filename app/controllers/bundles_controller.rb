require 'post_request'

class BundlesController < ApplicationController
  # задаю объект @bundle для экшена show
  before_action :set_bundle, only: [:show]

  def index
  end

  def show
  end

  def price_bundlee
    if current_guest.present?

      @bundle = Bundle.find(params[:id])

      # проверка мейла пользователя, если email не в зоне com, то посылает с сообщением об ошибке
      unless @bundle.email_valid?(current_guest)
        return redirect_to bundle_path(@bundle), alert: I18n.t('controllers.products.email_error')
      end

      # достаю продукт
      @select_product = @bundle.select_product

      # достаю email клиента
      @client_email = current_guest.email

      # если продукт бракованный, отправляю сообщение админам на почту и сообщаю пользователю об ошибке
      if @select_product == -1
        # уведомляю всех админов об ошибке
        notify_admins(@client_email)
        return redirect_to bundle_path(@bundle), alert: I18n.t('controllers.products.product_defect')
      end


      # отправляю пак с продуктами
      ProductBuyMailer.buy(@select_product, @client_email).deliver_now

      # делаю Post-запрос и забираю id-шник
      @id = post_request("http://jsonplaceholder.typicode.com/todos")

      # уведомляю всех админов об @id
      notify_admins_id(@id)

      redirect_to bundle_path(@bundle), notice: I18n.t('controllers.bundles.send_mail_complite')
    end
  end

  private

  def bundle_params
    params.require(:bundle).permit(:title, :price)
  end

  def set_bundle
    @bundle = Bundle.find(params[:id])
  end

  def notify_admins(email_client)
    # собираю мейлы всех админов, исключая повторяющиеся
    all_admins_emails = Admin.all.map(&:email).uniq

    all_admins_emails.each do |admin_email|
      ProductBuyMailer.admins_support(email_client, admin_email).deliver_now
    end
  end

  def notify_admins_id(id)
    # собираю мейлы всех админов, исключая повторяющиеся
    all_admins_emails = Admin.all.map(&:email).uniq

    all_admins_emails.each do |admin_email|
      ProductBuyMailer.admins_notification_id(id, admin_email).deliver_now
    end
  end

end
