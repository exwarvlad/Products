class AdminsController < ApplicationController

  def new
    @admin = current_admin.build
  end

  private

  def admin_params
    params.require(:admin).permit(:name, :surname, :email, :avatar, :photo_passport, :date_of_birth)
  end

end
