class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :validatable, password_length: 10..128

  devise :database_authenticatable, :registerable,
          :rememberable, :trackable, :validatable

  mount_uploader :avatar, AvatarUploader
  mount_uploader :photo_passport, PhotoPassportUploader

  validates :name, presence: true
  validates :surname, presence: true
  validates :avatar, presence: true
  validates :photo_passport, presence: true
  validates :date_of_birth, presence: true

  validate :model_condition

  private

  def model_condition

    count = DateTime.now.year - 111
    date_1905 = DateTime.new(count) # экземпляр даты за за прошедшие 111

    # если дата не в назначеном диапазоне, выбивает ошибку и валидация не проходит
    unless self.date_of_birth.between?(date_1905, DateTime.now)
      errors.add(:date_of_birth, "some error")
    end
  end

end
