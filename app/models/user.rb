class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :validatable, password_length: 8..128

  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable

  has_many :products
  has_many :bundles

  validates :name, presence: true, length: {maximum: 35}
  validates :email, presence: true
  validates :magaz_avatar, presence: true

  mount_uploader :magaz_avatar, MagazAvatarUploader
end
