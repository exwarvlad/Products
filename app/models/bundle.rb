require 'RandomUrl'

class Bundle < ActiveRecord::Base
  belongs_to :user
  has_many :products

  validates :user, presence: true
  validates :products, presence: true

  # проверка, что в паке больше одного продукта
  validates :valid_products_size?, presence: true

  # проверяет валидность мейла для покупки
  def email_valid?(user)
    guest_email = user.email
    last_3_symbol_email = guest_email.reverse[0..3].reverse

    # email валиден, если не заканчивается на .com
    if last_3_symbol_email == ".com"
      false
    else
      true
    end

  end

  # берет продукт, для отправки клиенту или возвращает -1 если продукт негоден
  def select_product
    # захожу в магазин
    product = RandomUrl.new

    # беру нужный мне продукт
    product.get_randoms_urls

    # проверяю на годность
    if product.valid_url?(product.random_url, product.thumbnail_url)
      product.random_url
    else
      -1
    end
  end

  private

  #
  def valid_products_size?
    if self.products.size > 1
      true
    else
      false
    end
  end

end
