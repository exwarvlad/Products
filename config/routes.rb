Rails.application.routes.draw do
  devise_for :guests, controllers: {sessions: 'application/sessions', registrations: 'application/registrations'}

  devise_for :admins, controllers: {sessions: 'application/sessions', registrations: 'application/registrations'}

  devise_for :users, controllers: {sessions: 'application/sessions', registrations: 'application/registrations'}

  root "products#index"

  resources :products do
    put 'admin', on: :member
    get 'price_product', on: :member
  end

  resources :bundles do
    get 'price_bundlee', on: :member
  end

end
