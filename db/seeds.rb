# беру фоточку для авки магазина
begin
f = File.open('app/assets/images/homer.png')
rescue Errno::ENOENT
  puts I18n.t('seed.homer')
  abort I18n.t('seed.advice')
end

# готовлю к созданию валадельца магазина
u = User.new(name: 'Магазин вкусняшек', email: 'trololo42@ya.ru', magaz_avatar: f, password: '12345678')

# останавливаю процесс, если этот user не валиден
abort I18n.t('seed.user_create_error') unless u.valid?

u.save
b = Bundle.new(user: u, title: 'Новогодние подарки', price: 10000)

begin
  # беру фоточки для товара
  f1 = File.open('app/assets/images/01_krasivaya-snegurochka.jpg')
  f2 = File.open('app/assets/images/02_krasivaya-snegurochka.jpg')
  f3 = File.open('app/assets/images/03_krasivaya-snegurochka.jpg')
  f4 = File.open('app/assets/images/04_krasivaya-snegurochka.jpg')
rescue Errno::ENOENT
  u.destroy if u.save # удаляю юзера, если он создался
  puts I18n.t('seed.files_not_found')
  abort I18n.t('seed.advice')
end

# наполняю пак товарами
b.products.new(user: u, title: 'Снегурочка', description: 'Красивая Снегурочка в красном', photo: f1)
b.products.new(user: u, title: 'Снегурочка', description: 'Красивая Снегурочка в красном', photo: f2)
b.products.new(user: u, title: 'Снегурочка', description: 'Красивая Снегурочка в синем', photo: f3)
b.products.new(user: u, title: 'Снегурочка', description: 'Красивая Снегурочка в белом', photo: f4)

b.save

puts I18n.t('seed.create_successful')

