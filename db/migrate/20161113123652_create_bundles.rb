class CreateBundles < ActiveRecord::Migration
  def change
    create_table :bundles do |t|
      t.string :title
      t.integer :price

      t.timestamps null: false
    end
    add_reference :products, :bundle, index: true, foreign_key: true
    add_reference :bundles, :user, index: true, foreign_key: true
  end
end
