class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins do |t|
      t.string :name
      t.string :surname
      t.string :avatar
      t.string :photo_passport
      t.datetime :date_of_birth

      t.timestamps null: false
    end
  end
end
